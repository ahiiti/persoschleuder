from flask import Flask, Response, render_template
from perso_utils.perso import Perso
import random
import json

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/values.json')
def values():
    perso = Perso()
    mr_area = perso.mr_area()
    data = {
        'label-id-number': perso.id_number(),
        'label-first-name': perso.first_name.upper(),
        'label-last-name': perso.last_name.upper(),
        'label-birth': f'{perso.birth[2]:02}.{perso.birth[1]:02}.{perso.birth[0]:04}',
        'label-birth-place': perso.birth_place.upper(),
        'label-expiry': f'{perso.expiry[2]:02}.{perso.expiry[1]:02}.{perso.expiry[0]:04}',
        'label-eye-color': perso.eye_color.upper(),
        'label-height': f'{perso.height} cm',
        'label-authority': perso.authority,
        'label-mr1': mr_area[0],
        'label-mr2': mr_area[1],
        'label-mr3': mr_area[2]
    }
    return Response(json.dumps(data), mimetype='application/json')


if __name__ == '__main__':
    app.run()